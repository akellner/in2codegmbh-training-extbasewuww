jQuery(document).ready(function() {
	$('input[data-autocompletion="1"]').keyup(function() {
		$.ajax({
			url: '/index.php?eID=addressAutoCompletion',
			data: 'tx_address_pi1[value]=' + $(this).val(),
			cache: false,
			success: function(data) {
				$('.autocompletioncontainer').remove();
				$('input[data-autocompletion="1"]').after(data);
			}
		});
	});
	$('body').on('click', '.autocompletioncontainer > li', function() {
		$('input[data-autocompletion="1"]').val($(this).html());
		$('.autocompletioncontainer').fadeOut('fast');
	});
});