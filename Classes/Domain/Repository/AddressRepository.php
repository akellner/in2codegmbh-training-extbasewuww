<?php
namespace In2code\Address\Domain\Repository;

class AddressRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Find addressed by Searchterm
	 *
	 * @param $sword
	 * @return query object
	 */
	public function findBySword($sword) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);

		// where
		$query->matching(
			$query->like('title', '%' . $sword . '%')
		);

		return $query->execute();
	}
}