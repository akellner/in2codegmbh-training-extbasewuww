<?php
namespace In2code\Address\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Alex Kellner <alexander.kellner@in2code.de>, in2code.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package address
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * addressRepository
	 *
	 * @var \In2code\Address\Domain\Repository\AddressRepository
	 * @inject
	 */
	protected $addressRepository;

	/**
	 * persistenceManager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;

	/**
	 * action list
	 *
	 * @param \string $sword
	 * @return void
	 */
	public function listAction($sword = NULL) {
		$addresses = $this->addressRepository->findBySword($sword);
		$this->view->assign('addresses', $addresses);
		$this->view->assign('sword', $sword);
	}

	/**
	 * action detail
	 *
	 * @param \In2code\Address\Domain\Model\Address $address
	 * @return void
	 */
	public function detailAction($address) {
		$this->view->assign('address', $address);
	}

	/**
	 * action list2
	 *
	 * @return void
	 */
	public function list2Action() {
		$addresses = $this->addressRepository->findAll();
		$this->view->assign('addresses', $addresses);
	}

	/**
	 * New
	 *
	 * @return void
	 */
	public function newAction() {
	}

	/**
	 * Create
	 *
	 * @param \In2code\Address\Domain\Model\Address $address
	 * @return void
	 */
	public function createAction($address) {
		$this->addressRepository->add($address);
		$this->flashMessageContainer->add('Datensatz erfolgreich erstellt');
		$this->redirect('new');
	}

	/**
	 * Edit
	 *
	 * @param \In2code\Address\Domain\Model\Address $address
	 * @return void
	 */
	public function editAction($address = NULL) {
		$this->view->assign('address', $address);
	}

	/**
	 * Update
	 *
	 * @param \In2code\Address\Domain\Model\Address $address
	 * @return void
	 */
	public function updateAction($address) {
		$this->addressRepository->update($address);
		$this->flashMessageContainer->add('Datensatz erfolgreich geändert');
		$this->redirect('list');
	}

	/**
	 * Autocompletion via AJAX and eID
	 *
	 * @param \string $value
	 * @return void
	 */
	public function autoCompletionAction($value) {
		$addresses = $this->addressRepository->findBySword($value);
		$this->view->assign('addresses', $addresses);
	}

	/**
	 * Deactivate systemerrors in flashmessages
	 *
	 * @return bool
	 */
	protected function getErrorFlashMessage() {
		return FALSE;
	}

}