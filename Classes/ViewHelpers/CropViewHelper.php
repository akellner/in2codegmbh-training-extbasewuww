<?php
namespace In2code\Address\ViewHelpers;

/**
 * Crop
 *
 * @return \string
 */
class CropViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Crop
	 *
	 * @param \string $string
	 * @param \int $length
	 * @return \string
	 */
	public function render($string, $length = 20) {
		if (strlen($string) > $length) {
			$string = substr($string, 0, $length) . '...';
		}
		return $string;
	}
}