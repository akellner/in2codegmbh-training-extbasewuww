<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'In2code.' . $_EXTKEY,
	'Pi1',
	array(
		'Address' => 'list,detail,list2,new,create,edit,update,autoCompletion',
	),
	// non-cacheable actions
	array(
		'Address' => 'list,detail,list2,new,create,edit,update,autoCompletion',
	)
);

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['addressAutoCompletion'] = 'EXT:address/Classes/Utility/EidAutoCompletion.php';